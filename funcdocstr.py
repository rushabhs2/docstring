#Docstrings for Python Functions#

#The docstring for a function or method should summarize its behavior and document its arguments and return values.
#It should also list all the exceptions that can be raised and other optional arguments.

########################################################################################################################

#Example-1#
def add_binary(a, b):
    '''
    Returns the sum of two decimal numbers in binary digits.

            Parameters:
                    a (int): A decimal integer
                    b (int): Another decimal integer

            Returns:
                    binary_sum (str): Binary string of the sum of a and b
    '''
    binary_sum = bin(a+b)[2:]
    return binary_sum


print(add_binary.__doc__)

########################################################################################################################

#Example2#
def cook_access_token(identity: str) -> str:
    """
    This function is a wrapper for the jwt.encode entity.

    :param identity: String to be used in the token parameter 'sub'.
    :return: A JSON WEB TOKEN of access type.
    """
    claim = {'sub': identity}
    access_token = jwt.encode(claim, SECRET_KEY, ALGORITHM)
    return access_token

########################################################################################################################

#Example3#
def check_token(in_token: str) -> bool:
    """
    This function is a wrapper for the jwt.decode entity.

    :param in_token: Token received in the request by user.
    :return: True if the token is authentic, else False.
    """
    decoded_in_token = jwt.decode(in_token, SECRET_KEY, ALGORITHM)

    if decoded_in_token.get('sub') is None:
        return False
    else:
        return True

########################################################################################################################
